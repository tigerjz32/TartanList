from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.forms import PasswordResetForm
from django.forms.widgets import DateInput
from models import *
from sqlite3 import Date

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# item
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        exclude = ('user','latitude','longitude')
        widgets = {'picture' : forms.FileInput(), 
                    'title' : forms.TextInput(attrs={'class':'form-control','placeholder':'Title', 'required':'' }),
                    'description' : forms.Textarea(attrs={'class':'form-control','placeholder':'Description', 'required':'' }),
                    'address' : forms.TextInput(attrs={'class':'form-control','placeholder':'Please Enter as Street Address, Area, State'}),
                    'itemURL' : forms.TextInput(attrs={'class':'form-control','placeholder':'External URL'}),
                    'category' : forms.Select(attrs={'class':'form-control','placeholder':'Please select a category...', 'required':''}),
                    'price' : forms.TextInput(attrs={'class':'form-control','placeholder':'Price (set price to 0.0 for Best Offer)'}),
                    'sellBy' : forms.DateInput(attrs={'class':'form-control', 'placeholder': 'Sell By'})}
   
    def clean(self):
        errors = []
        cleaned_data = super(ItemForm, self).clean()
        
        title = self.cleaned_data.get('title')
        description = self.cleaned_data.get('description')
        address = self.cleaned_data.get('address')
        itemURL = self.cleaned_data.get('itemURL')
        category = self.cleaned_data.get('category')
        price = self.cleaned_data.get('price')
        sellBy = self.cleaned_data.get('sellBy')
        
        if not title or not description or not category:
            errors.append("Please be sure to fill out all the fields!")
        if price < 0.0:
            errors.append("Price has to be a valid positive number!")
        if itemURL:
            if not (itemURL.startswith('http:') or itemURL.startswith('https:')):
                errors.append("Please enter a full URL! For example: http://www.google.com") 
        if sellBy:
            if sellBy < Date.today():    
                errors.append("Sell By date cannot be in the past!")
        if errors:
            raise forms.ValidationError(errors)  
        else:
            return cleaned_data


class SearchForm(forms.Form):
    search = forms.CharField(max_length = 100,
    widget = forms.TextInput(attrs={'class':'form-control','type':'text','required':''}))

    def clean(self):
        cleaned_data = super(SearchForm, self).clean()

        search = self.cleaned_data.get('search')

        if search == "" or not search:
            raise forms.ValidationError("Cannot be empty!")
                     
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# login and registration
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

class RegistrationForm(forms.Form):
    username = forms.CharField(max_length = 20, widget = forms.TextInput(attrs={'class':'form-control','placeholder':'Username','required':''}))
    email = forms.EmailField(max_length=200, widget = forms.TextInput(attrs={'class':'form-control','placeholder':'Email','required':''}))
    first_name = forms.CharField(max_length = 200, widget = forms.TextInput(attrs={'class':'form-control','placeholder':'First Name','required':''}))
    last_name = forms.CharField(max_length = 200, widget = forms.TextInput(attrs={'class':'form-control','placeholder':'Last Name','required':''}))
    password1 = forms.CharField(max_length = 200,label = 'Password', widget = forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password','required':''}))
    password2 = forms.CharField(max_length = 200,label = 'Confirm Password', widget = forms.PasswordInput(attrs={'class':'form-control','placeholder':'Confirm Password','required':''}))
    
    def clean(self):
        errors = []
        cleaned_data = super(RegistrationForm, self).clean()
        
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        
        if password1 != password2:
            errors.append("Passwords do not match")
        if User.objects.filter(username__exact = username):
            errors.append("Username is already taken")
        if User.objects.filter(email__exact = email):
            errors.append("Email address is already taken!")
        if not "@andrew.cmu.edu" in email and not "@cmu.edu" in email:
            errors.append("Email address can only be for a CMU member")
        if not any(x.isupper() for x in password1) or not any(x.islower() for x in password1) or not any(x.isdigit() for x in password1) or len(password1) <= 8:
            errors.append("Please make sure the password is longer than 8 chars, has 1 digits, has 1 upper case, and has 1 lower")
        if not username or not email or not first_name or not last_name or not password1 or not password2:
            errors.append("Please be sure to fill all the fields") 
               
        if errors:
            raise forms.ValidationError(errors)  
        else:
            return cleaned_data
        
class ResetPasswordForm(forms.Form):
    email = forms.EmailField(max_length=200, widget = forms.TextInput(attrs={'class':'form-control','placeholder':'Email','required':''}))

    def clean(self):
        cleaned_data = super(ResetPasswordForm, self).clean()

        email = self.cleaned_data.get('email')
        if email == "" or not email:
            raise forms.ValidationError("Please Enter an Email ID!")   
        return cleaned_data

class EnterNewPasswordForm(forms.Form):
    password2 = forms.CharField(max_length=200,label = 'New Password', widget = forms.PasswordInput(attrs={'class':'form-control','placeholder':'New Password','required':''}))
    password3 = forms.CharField(max_length=200,label = 'Confirm Password', widget = forms.PasswordInput(attrs={'class':'form-control','placeholder':'Confirm New Password','required':''}))
    
    def clean(self):
        cleaned_data = super(EnterNewPasswordForm, self).clean()

        password2 = self.cleaned_data.get('password2')
        password3 = self.cleaned_data.get('password3')
        if not password2 or not password3:
            raise forms.ValidationError("Password fields cannot be left empty!")
        if password2 != password3:
            raise forms.ValidationError("New Passwords do not match, please try again.")
        if not any(x.isupper() for x in password2) or not any(x.islower() for x in password2) or not any(x.isdigit() for x in password2) or len(password2) <= 8:
            raise forms.ValidationError("Please make sure the password is longer than 8 chars, has 1 digits, has 1 upper case, and has 1 lower")
        return cleaned_data
        
class EditNameForm(forms.Form):
    first_name = forms.CharField(max_length=100, widget = forms.TextInput(attrs={'class':'form-control','placeholder':'First Name','required':''}))
    last_name = forms.CharField(max_length=100, widget = forms.TextInput(attrs={'class':'form-control','placeholder':'Last Name','required':''}))
    
    def clean(self):
        cleaned_data = super(EditNameForm, self).clean()

        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        
        if first_name == '' or not first_name or last_name == '' or not last_name:
            raise forms.ValidationError('Please make sure all fields are filled')
            
        return cleaned_data
        
class EditPasswordForm(forms.Form):
    oldPassword = forms.CharField(max_length = 200,label = 'Old Password', widget = forms.PasswordInput(attrs={'class':'form-control','placeholder':'Old Password','required':''}))
    newPassword = forms.CharField(max_length = 200,label = 'New Password', widget = forms.PasswordInput(attrs={'class':'form-control','placeholder':'New Password','required':''}))
    confirmPassword = forms.CharField(max_length = 200,label = 'Confirm New Password', widget = forms.PasswordInput(attrs={'class':'form-control','placeholder':'Confirm Password','required':''}))
                   
    def clean(self):
        cleaned_data = super(EditPasswordForm, self).clean()
        
        oldPassword = cleaned_data.get('oldPassword')
        newPassword = cleaned_data.get('newPassword')
        confirmPassword = cleaned_data.get('confirmPassword')
        
        if newPassword != confirmPassword or oldPassword == "" or not oldPassword or newPassword == "" or not newPassword or confirmPassword == "" or not confirmPassword:
            raise forms.ValidationError("Please make sure that the passwords match and all fields are filled!") 
        if not any(x.isupper() for x in newPassword) or not any(x.islower() for x in newPassword) or not any(x.isdigit() for x in newPassword) or len(newPassword) <= 8:
            raise forms.ValidationError("Please make sure the password is longer than 8 chars, has 1 digits, has 1 upper case, and has 1 lower")    
        return cleaned_data

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# filtering and sorting
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

FILTER_ITEM_CHOICES = (('Appliances','Appliances'),
                        ('Automotive','Automotive'),
                        ('Books','Books'),
                        ('Phones','Phones'),
                        ('Clothing','Clothing'),
                        ('Computers','Computers'),
                        ('Electronics','Electronics'),
                        ('Home','Home'),
                        ('Rental','Rental'),
                        ('Travel','Travel'),
                        ('Office','Office'),
                        ('Outdoors','Outdoors'),
                        ('Other','Other'))
class FilterForm(forms.Form):
    filter_items = forms.MultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple, 
        choices=FILTER_ITEM_CHOICES,
        initial=['Appliances','Automotive','Books','Phones','Clothing','Computers','Electronics','Home','Rental','Travel','Office','Outdoors','Other'])
    
    def clean(self):
        cleaned_data = super(FilterForm, self).clean()
        return cleaned_data
    
class PriceForm(forms.Form):
    min = forms.DecimalField(widget = forms.TextInput(attrs={'class':'form-control','placeholder':'$ Min.'}))
    max = forms.DecimalField(widget = forms.TextInput(attrs={'class':'form-control','placeholder':'$ Max.'}))
    
    def clean(self):
        errors = []
        cleaned_data = super(PriceForm, self).clean()

        min = cleaned_data.get('min')
        max = cleaned_data.get('max')
        
        if int(min) > int(max):
            errors.append("Minimum value cannot be larger than maximum!") 
        elif min < 0.0 or max < 0.0:
            errors.append("Values must be positive!")
            
        if errors:
            raise forms.ValidationError(errors)  
        else:
            return cleaned_data

SORT_ITEM_CHOICES = (('TitleAZ','Title: A to Z'),
                    ('TitleZA','Title: Z to A'),
                    ('PriceAZ','Price: Lowest to Highest'),
                    ('PriceZA','Price: Highest to Lowest'),
                    ('SellByAZ','Sell by: Earliest to Latest'),
                    ('SellByZA','Sell by: Latest to Earliest'))
class SortForm(forms.Form):
    sortItems = forms.ChoiceField(required=False, choices=SORT_ITEM_CHOICES,
    widget=forms.Select(attrs={'class':'form-control','type':'button','data-toggle':'dropdown'}))
    
    def clean(self):
        cleaned_data = super(SortForm, self).clean()
        return cleaned_data
