from django.db import models
from django.contrib.auth.models import User

class Item(models.Model):
    
    CATEGORIES = (
        ('Appliances', 'Appliances'),
        ('Automotive', 'Automotive'),
        ('Books', 'Books'),
        ('Phones', 'Phones'),
        ('Clothing', 'Clothing'),
        ('Computers', 'Computers'),
        ('Electronics', 'Electronics'),
        ('Home', 'Home'),
        ('Rental', 'Rental'),
        ('Travel', 'Travel'),
        ('Office', 'Office'),
        ('Outdoors', 'Outdoors'),
        ('Other', 'Other'),
    )
    title = models.CharField(max_length=200)
    description  = models.CharField(max_length=5000)
    address = models.CharField(max_length=500, blank=True, default=None)
    itemURL = models.CharField(max_length=200, null=True, blank=True)
    category = models.CharField(max_length=100, choices=CATEGORIES, default='Other')
    sellBy = models.DateField(null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    user = models.ForeignKey(User)
    picture = models.ImageField(upload_to="item_photos", blank=True)
    latitude = models.CharField(max_length=200)
    longitude = models.CharField(max_length=200)
    def __unicode__(self):
        return unicode(self.title) or u''

class SoldItem(models.Model):
    item = models.ForeignKey(Item)
    user = models.ForeignKey(User)
    def __unicode__(self):
        return unicode(self.item) or u''
        
class ExpressedInterestItem(models.Model):
    item = models.ForeignKey(Item)
    user = models.ForeignKey(User)
    def __unicode__(self):
        return unicode(self.item) or u''