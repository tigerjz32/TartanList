from django.test import TestCase, Client
from models import *
from sqlite3 import Date

class TartanListModelsTest(TestCase):
   def test_simple_add(self):
        self.user = User.objects.create_user(username="test", password="test")
        self.assertTrue(Item.objects.all().count() == 0)
        new_item = Item(title="Test Post #1", description="Test Post #1 Body", address = '147 nc' , sellBy = Date.today(), category = 13, price = 0, user = self.user)
        new_item.save()
        self.assertTrue(Item.objects.all().count() == 1)
        self.assertTrue(Item.objects.filter(title__contains='test'))

class TartanListTest(TestCase):
    
    # Initial setup
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username="test", password="test")
        self.item = Item.objects.create(title="Test Post #1", description="Test Post #1 Body", address = '147 nc' , sellBy = Date.today(), category = 13, price = 0, user = self.user)

    # Testing to see if the pages exist
    def test_home_page(self):   
        client = Client()       
        client.login(username='test', password='test')
        response = client.get('/home')
        self.assertEqual(response.status_code, 200)
    def test_profile_page(self):  
        client = Client()       
        client.login(username='test', password='test')
        response = client.get('/profile')
        self.assertEqual(response.status_code, 200)
    def test_edit_page(self):   
        client = Client()       
        client.login(username='test', password='test')
        response = client.get('/edit')
        self.assertEqual(response.status_code, 200)   
    def test_search_page(self):   
        client = Client()       
        client.login(username='test', password='test')
        response = client.get('/search')
        self.assertEqual(response.status_code, 200)
    def test_add_item_page(self):   
        client = Client()       
        client.login(username='test', password='test')
        response = client.get('/item-add')
        self.assertEqual(response.status_code, 200)
    def test_item_page(self):   
        client = Client()       
        client.login(username='test', password='test')
        response = client.get('/item-detail/1')
        self.assertEqual(response.status_code, 200)
    def test_edit_item_page(self):   
        client = Client()       
        client.login(username='test', password='test')
        response = client.get('/item-edit/1')
        self.assertEqual(response.status_code, 200)
    
    # Testing to see if we can create an item
    def test_item_creation(self): 
        self.assertEqual(self.item.title, "Test Post #1")
        self.assertEqual(self.item.description, "Test Post #1 Body")
        self.assertEqual(self.item.address, "147 nc")
        self.assertEqual(self.item.sellBy, Date.today())
        self.assertEqual(self.item.category, 13)
        self.assertEqual(self.item.user, self.user)    
        
    # Testing to see if Item Add function is working
    def test_item_add_new(self):    
        client = Client()       
        client.login(username='test', password='test')
        response = client.post('/item-add-new', {'title':self.item.title, 'description': self.item.description, 'address': self.item.address, 'sellBy': self.item.sellBy, 'category': self.item.category, 'user':self.item.user })
        self.assertTrue(response.content.find(self.item.title) >= 0)
        
    # Testing to see if Item Delete function is working
    def test_item_delete(self):    
        client = Client()       
        client.login(username='test', password='test')
        response = client.post('/item-add-new', {'title':self.item.title, 'description': self.item.description, 'address': self.item.address, 'sellBy': self.item.sellBy, 'category': self.item.category, 'user':self.item.user })
        response = client.post('/item-delete/1')
        self.assertTrue(response.content.find(self.item.title) <= 0)    
    
    # Testing to see if Item Edit Function is working
    def test_item_edit(self):    
        client = Client()       
        client.login(username='test', password='test')
        new_title = 'new title'
        response = client.post('/item-add-new', {'title':self.item.title, 'description': self.item.description, 'address': self.item.address,'sellBy': self.item.sellBy, 'category': self.item.category, 'user':self.item.user })
        response = client.post('/item-edit/1', {'title':new_title, 'description': self.item.description, 'address': self.item.address, 'sellBy': self.item.sellBy, 'category': self.item.category, 'user':self.item.user })
        self.assertTrue(new_title in response.content)
        
    # Testing to see if Search Function is working
    def test_item_search(self):    
        client = Client()       
        client.login(username='test', password='test')
        search = 'sofa'
        response = client.post('/item-add-new', {'title':search, 'description': self.item.description, 'address': self.item.address, 'sellBy': self.item.sellBy, 'category': self.item.category, 'user':self.item.user })
        response = client.get('/search', {'search':search})
        self.assertTrue(search in response.content)
        
    