from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # home & profile 
    url(r'^$', 'TartanList.views.home', name='home'),
    url(r'^home$', 'TartanList.views.home', name='home'),
    url(r'^cover$', 'TartanList.views.cover', name='cover'),
    url(r'^profile$', 'TartanList.views.profile', name='profile'),
    url(r'^search', 'TartanList.views.search', name='search'),
    
    #login and registration 
    url(r'^login$', 'django.contrib.auth.views.login', {'template_name':'login.html'}, name = 'login'),
    url(r'^logout$', 'django.contrib.auth.views.logout_then_login', name = 'logout'),
    url(r'^register$', 'TartanList.views.register', name='register'),
    url(r'^confirm-registration/(?P<username>[a-zA-Z0-9_@\+\-]+)/(?P<token>[a-z0-9\-]+)$', 'TartanList.views.confirm_registration', name='confirm'),
    url(r'^reset$', 'TartanList.views.resetPassword', name='reset'),
    url(r'^confirm-email/(?P<username>[a-zA-Z0-9_@\+\-]+)/(?P<token>[a-z0-9\-]+)$', 'TartanList.views.confirm_email', name='confirm_email'),
    url(r'^enter-new-password$', 'TartanList.views.enter_new_password', name='enter-new-password'),
    
    #items url
    url(r'^item-add$', 'TartanList.views.item_add', name='item-add'),
    url(r'^item-add-new$', 'TartanList.views.item_add_new', name='item-add-new'),
    url(r'^item-photos/(?P<id>\d+)$', 'TartanList.views.item_photo', name='item-photo'),
    url(r'^item-sold/(?P<id>\d+)$', 'TartanList.views.item_sold', name='item-sold'),
    url(r'^item-delete/(?P<id>\d+)$', 'TartanList.views.item_delete', name='item-delete'),
    url(r'^item-detail/(?P<id>\d+)$', 'TartanList.views.item_detail', name='item-detail'),
    url(r'^item-edit/(?P<id>\d+)$', 'TartanList.views.item_edit', name='item-edit'),
    url(r'^item-contact/(?P<id>\d+)$', 'TartanList.views.item_contact', name='item-contact'),
   
    #edit url
    url(r'^edit$', 'TartanList.views.edit', name='edit'),
    url(r'^edit_name$', 'TartanList.views.edit_name', name='edit_name'),
    url(r'^edit_password$', 'TartanList.views.edit_password', name='edit_password'),
    url(r'^search$', 'TartanList.views.search', name='search'),
    
    #sort & filter
    url(r'^sort-filter$', 'TartanList.views.sort_filter', name='sort-filter')
)