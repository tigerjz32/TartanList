from django.shortcuts import render_to_response
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.db import transaction

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate

from TartanList.models import *
from TartanList.forms import *
from mimetypes import guess_type

from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
from django.http import HttpResponse, Http404
from django.core import serializers

from django.db.models import Max
from django.db.models import Count
from decimal import Decimal

import simplejson, urllib
import requests
import json

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# home and profile
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@login_required
def home(request):
    context = {}
    sold = SoldItem.objects.all().values('item_id')
    context['items'] = Item.objects.all().exclude(id__in=sold).exclude(user=request.user)
    context['SearchForm'] = SearchForm()
    context['FilterForm'] = FilterForm()
    context['PriceForm'] = PriceForm()
    context['SortForm'] = SortForm()
    return render(request, 'home.html', context)

def cover(request):
    return render_to_response('cover.html')

@login_required
def profile(request):
    context = {}
    context['SearchForm'] = SearchForm()
    sold = SoldItem.objects.filter(user=request.user).values('item_id')
    expressed = ExpressedInterestItem.objects.filter(user=request.user).values('item_id')
    context['expressed'] = Item.objects.filter(id__in=expressed)
    context['sold'] = Item.objects.filter(id__in=sold)
    context['items'] = Item.objects.filter(user=request.user).exclude(id__in=sold)

    return render(request, 'profile.html', context)

@login_required
def search(request):
    context = {}

    if request.method == 'POST':
        return redirect('home')

    context['SearchForm'] = SearchForm(request.GET)

    if not context['SearchForm'].is_valid():
        return render(request,'search.html',context)

    sold = SoldItem.objects.all().values('item_id')
    context['items'] = Item.objects.filter(title__icontains=context['SearchForm'].cleaned_data['search']).exclude(id__in=sold).exclude(user=request.user)
    context['search_value'] = str(context['SearchForm'].cleaned_data['search'])

    for cat in context['items'].values('category').annotate(total=Count('category')):
        context[str(cat['category'])] = cat['total']

    return render(request, 'search.html', context)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# login and registration
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

def register(request):
    context = {}

    if request.method == 'GET':
        context['RegistrationForm'] = RegistrationForm()
        return render(request, 'register.html', context)

    form = RegistrationForm(request.POST)
    context['RegistrationForm'] = form

    # Validates the form.
    if not form.is_valid():
        return render(request, 'register.html', context)

    new_user = User.objects.create_user(username = form.cleaned_data['username'], 
                                        password = form.cleaned_data['password1'],
                                        email = form.cleaned_data['email'],
                                        first_name = form.cleaned_data['first_name'],
                                        last_name = form.cleaned_data['last_name'])

    new_user.is_active = False
    new_user.save()

    token = default_token_generator.make_token(new_user)

    email_body = """
    Welcome to the Tartan's List """ + new_user.first_name + """! 

        Username: """ + new_user.username + """

    Please click the link below to verify your email address and complete the registration of your account:
    
        http://%s%s
    """ % (request.get_host(), 
    reverse('confirm', args=(new_user.username, token)))

    send_simple_message(request, new_user, email_body)

    context['email'] = form.cleaned_data['email']
    return render(request, 'needs-confirmation.html', context)
    

def confirm_registration(request, username, token):
    context = {}
    user = get_object_or_404(User, username=username)

    if not default_token_generator.check_token(user, token):
        raise Http404

    user.is_active = True
    user.save()
    context['welcome'] = "Thank you for confirming your email address."
    return render(request, 'login.html', context)
     
    
def resetPassword(request):
    context = {}

    if request.method == 'GET':
        context['ResetPasswordForm'] = ResetPasswordForm()
        return render(request, 'reset-password.html', context)

    form = ResetPasswordForm(request.POST)
    context['ResetPasswordForm'] = form
    
    if not form.is_valid():
        return render(request, 'reset_password.html', context)
    
    email = form.cleaned_data['email']
    current_user = User.objects.filter(email__exact = email)[0]

    token = default_token_generator.make_token(current_user)

    email_body = """  
    Please click the link below to
    verify your email address and complete the password reset:

      http://%s%s
    """ % (request.get_host(), 
    reverse('confirm_email', args=(current_user.username, token)))

    send_simple_message(request, current_user, email_body)

    context['email'] = form.cleaned_data['email']
    return render(request, 'needs-confirmation.html', context)
    
def confirm_email(request, username, token):
    context = {}
    user = get_object_or_404(User, username=username)

    # Send 404 error if token is invalid
    if not default_token_generator.check_token(user, token):
        raise Http404

    context['welcome'] = "Your email has been confirmed, please enter a new password."
    context['EnterNewPasswordForm'] = EnterNewPasswordForm()
    context['email'] = user.email
    return render(request, 'enter-new-password.html', context) 

def enter_new_password(request):
    context = {}

    if request.method == 'GET':
        return redirect('login')

    form = EnterNewPasswordForm(request.POST)
    context['EnterNewPasswordForm'] = form

    if not form.is_valid():
        return render(request, 'enter-new-password.html', context)
        
    email = request.POST['email']
    u = User.objects.filter(email__exact = email)[0]
    new_password = form.cleaned_data['password3']
    u.set_password(new_password)
    u.save() 

    context['welcome'] = "Your password has been updated, please login."
    return render(request, 'login.html', context)    

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# items
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 

@login_required    
def item_add(request):
    context = {}
    context['SearchForm'] = SearchForm()
    current_user = request.user
    context['ItemForm'] = ItemForm()
    context['expressed'] = ''
    sold = current_user.solditem_set.all()
    context['sold'] = Item.objects.filter(id__in=sold)
    context['items'] = Item.objects.filter(user=request.user).exclude(id__in=sold.values('item_id'))
    return render(request, 'item-add.html', context)

@login_required
@transaction.atomic
def item_add_new(request):
    context = {}
    context['SearchForm'] = SearchForm()
    if request.method == 'GET':
        return redirect('item-add')

    new_item = Item(user=request.user)
    form = ItemForm(request.POST, request.FILES, instance=new_item)
    context['ItemForm'] = form

    if not form.is_valid():
        return render(request, 'item-add.html', context)
        
    address = str(form.cleaned_data['address'])
    if address:
        address_string = address.replace(" ","+")

        url = "https://maps.googleapis.com/maps/api/geocode/json?address="+address_string+"&key=##########"

        #cite the url from google web services website in the READ ME - Laura will do it
        result = simplejson.load(urllib.urlopen(url))
        l1 =  simplejson.dumps([s['geometry']['location']['lat'] for s in result['results']], indent=2)
        l2 =  simplejson.dumps([s['geometry']['location']['lng'] for s in result['results']], indent=2)

        lat = l1.translate(None, '[]').replace(' ','')
        lng = l2.translate(None, '[]').replace(' ','')

    if address:
        new_item = Item(user = request.user, latitude = lat.split(',')[0], longitude = lng.split(',')[0])
        form = ItemForm(request.POST, request.FILES, instance=new_item)
        if not form.is_valid():
            return render(request, 'item-add.html', context)

    form.save()
    return redirect('profile')

@login_required
@transaction.atomic
def item_photo(request, id):
    item = Item.objects.get(id__exact=id)
    entry = get_object_or_404(Item, id=id)

    if not entry.picture:
        raise Http404

    content_type = guess_type(entry.picture.name)
    return HttpResponse(entry.picture, content_type=content_type)

@login_required
def item_sold(request, id):
    item = Item.objects.get(id__exact=id)
    user = request.user
    
    sold = SoldItem.objects.filter(user=user, item=item)
    if not sold:
        new_soldItem = SoldItem(item=item,user=user)
        new_soldItem.save()

    return redirect('profile')
       
@login_required
def item_delete(request, id):
    context = {}
    try:
        item_to_delete = Item.objects.get(id=id, user=request.user)
        item_to_delete.delete()
    except ObjectDoesNotExist:
        context['errors'] = 'The item does not exist!'

    return redirect('edit')

@login_required
def item_detail(request, id):
    context={}
    context['SearchForm'] = SearchForm()
    context['item'] = Item.objects.get(id__exact=id)
    context['itemUser'] = context['item'].user

    return render(request, 'item.html', context)

@login_required
def item_edit(request, id):
    context = {}
    context['SearchForm'] = SearchForm()
    item_to_edit = Item.objects.get(id__exact=id)
    context['item_id'] = id

    if request.user != item_to_edit.user or SoldItem.objects.filter(item_id=id):
        return redirect('home')
    
    if request.method == 'GET':
        context['EditItemForm'] = ItemForm(instance=item_to_edit)
        return render(request, 'edit-item.html', context)

    form = ItemForm(request.POST, request.FILES, instance = item_to_edit)
    context['EditItemForm'] = form
    
    if not form.is_valid():
        return render(request, 'edit-item.html', context)
        
    address = str(form.cleaned_data['address'])
    address_string = address.replace(" ","+")
    url = "https://maps.googleapis.com/maps/api/geocode/json?address="+address_string+"&key=##########"

    #cite the url from google web services website in the READ ME - Laura will do it
    result = simplejson.load(urllib.urlopen(url))
    l1 =  simplejson.dumps([s['geometry']['location']['lat'] for s in result['results']], indent=2)
    l2 =  simplejson.dumps([s['geometry']['location']['lng'] for s in result['results']], indent=2)
 
    lat = l1.translate(None, '[]').replace(' ','')
    lng = l2.translate(None, '[]').replace(' ','')
    
    item_to_edit = form.save( commit=False )
    item_to_edit.latitude = lat.split(',')[0]
    item_to_edit.longitude = lng.split(',')[0]
    item_to_edit.save()
    return redirect('edit')

@login_required
def item_contact(request, id):
    context={}
    context['SearchForm'] = SearchForm()
    context['item'] = Item.objects.get(id__exact=id)
    context['user'] = context['item'].user
    context['warning'] = "You've already contacted the user for " + context['item'].title + ". Please wait for a reply..." 
    expressed = ExpressedInterestItem.objects.filter(item=context['item'],user=request.user)
    message = "Hi " + context['item'].user.first_name  + ", We are contacting you because " + request.user.first_name + " has expressed interest in " + context['item'].title + ". You can contact " + request.user.first_name + " at "  + request.user.email + ". Thank you! Message from " + request.user.first_name + " : " + str(request.POST['personal']) 
    
    if not expressed:
        send_simple_message(request, context['item'].user, message)
        context['success'] = "An email has been sent to the seller with your contact details - " + request.user.email + "."
        new_expressed = ExpressedInterestItem(item=context['item'],user=request.user)
        new_expressed.save()

    return render(request, 'item.html', context)

def send_simple_message(request, user, message):
    return requests.post(
        "https://api.mailgun.net/v2/##########.mailgun.org/messages",
        auth=("api", "##########"),
        data={"from": "Tartan's List <postmaster@sandbox1d874c49d52647f0803c618009375a64.mailgun.org>",
              "to": user.first_name + "<" + user.email + ">",
              "subject": "Hello " + user.first_name,
              "text": message})

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# edit
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@login_required
def edit(request):
    context = {}
    context['SearchForm'] = SearchForm()
    sold = SoldItem.objects.all().values('item_id')
    context['items'] = Item.objects.filter(user=request.user).exclude(id__in=sold)
    context['EditNameForm'] = EditNameForm()
    context['EditPasswordForm'] = EditPasswordForm()
    return render(request, 'edit.html', context)
 
@login_required   
def edit_name(request):
    context = {}
    context['SearchForm'] = SearchForm()
    context['EditPasswordForm'] = EditPasswordForm()

    # Just display the registration form if this is a GET request.
    if request.method == 'GET':
        context['EditNameForm'] = EditNameForm()
        return redirect('edit')

    # Creates a bound form from the request POST parameters and makes the 
    # form available in the request context dictionary.
    form = EditNameForm(request.POST)
    context['EditNameForm'] = form

    # Validates the form.
    if not form.is_valid():
        return render(request, 'edit.html', context)
        
    first_name = form.cleaned_data['first_name']
    last_name = form.cleaned_data['last_name']
    u = request.user
    u.first_name = first_name
    u.last_name = last_name
    u.save() 
    return redirect('edit')
    
@login_required   
def edit_password(request):
    context = {}
    context['SearchForm'] = SearchForm()
    context['EditNameForm'] = EditNameForm()

    if request.method == 'GET':
        context['EditPasswordForm'] = EditPasswordForm()
        return redirect('edit')

    form = EditPasswordForm(request.POST)
    context['EditPasswordForm'] = form

    if not form.is_valid():
        return render(request, 'edit.html', context)
    
    oldPassword = form.cleaned_data['oldPassword']
    if request.user.check_password(oldPassword):
        u = request.user
        newPassword = form.cleaned_data['confirmPassword']
        u.set_password(newPassword)
        u.save()
        return redirect('login')
    else:
        context['errors'] = 'Old password is incorrect'
        return render(request, 'edit.html', context)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# sort & filter
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@login_required
def sort_filter(request):
    context = {}
    context['SearchForm'] = SearchForm()
    sold = SoldItem.objects.all().values('item_id')
    context['items'] = Item.objects.all().exclude(id__in=sold).exclude(user=request.user)

    # filter will error if no items are found to avoid this, just redirect to home
    if not context['items']:
        return redirect('home')
    
    if not request.GET['min'] and not request.GET['max']:
        max = context['items'].aggregate(Max('price'))
        dict = {'max':Decimal(str(max['price__max'])),
                'min':0.0}
        request.GET = request.GET.copy()
        request.GET.update(dict)
    elif not request.GET['min'] and request.GET['max']:
        dict = {'max':request.GET['max'],
                'min':0.0}
        request.GET = request.GET.copy()
        request.GET.update(dict)
        max = context['items'].aggregate(Max('price'))
        request.GET['max'] = Decimal(str(max['price__max']))  
    elif request.GET['min'] and not request.GET['max']:
        max = context['items'].aggregate(Max('price'))
        dict = {'max':Decimal(str(max['price__max'])),
                'min':request.GET['min']}
        request.GET = request.GET.copy()
        request.GET.update(dict)
        
    context['PriceForm'] = PriceForm(request.GET)
    context['FilterForm'] = FilterForm(request.GET)
    context['SortForm'] = SortForm(request.GET)

    if not context['FilterForm'].is_valid() or not context['PriceForm'].is_valid() or not context['SortForm'].is_valid():
        return render(request, 'home.html', context)

    sortitem = context['SortForm'].cleaned_data['sortItems']
    minprice = context['PriceForm'].cleaned_data['min']
    maxprice = context['PriceForm'].cleaned_data['max']
    categories = []

    for category in context['FilterForm'].cleaned_data['filter_items']:
        categories.append(str(category))
   
    context['items'] = context['items'].filter(category__in=categories).exclude(price__gt=maxprice).exclude(price__lt=minprice)

    if sortitem == 'TitleAZ':
        context['items'] = context['items'].order_by('title')
    elif sortitem == 'TitleZA':
        context['items'] = context['items'].order_by('title').reverse()
    elif sortitem == 'PriceAZ':
        context['items'] = context['items'].order_by('price')
    elif sortitem == 'PriceZA':
        context['items'] = context['items'].order_by('price').reverse()
    elif sortitem == 'SellByAZ':
        context['items'] = context['items'].order_by('sellBy')
    elif sortitem == 'SellByZA':
        context['items'] = context['items'].order_by('sellBy').reverse()
    
    #lists = []
    #return HttpResponse(json.dumps(lists), content_type="application/json")
    return render(request, 'home.html', context) 
