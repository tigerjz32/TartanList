Project proposal feedback
==================

**Team number**: 73<br>
**Team name**: Django Unchained<br>
**Team members**: jkansagr, lvas

### Arthur
This is a good project proposal and can be scaled up to involve many challenging problems. Storing, displaying, and manipulating location information, messaging systems, and useful searching/filtering would be features I would like you to heavily invest your time in, to ensure that your project deviates enough from being just a variation of Grumblr.

### Salem
This seems like a pretty appropriate amount of work for two people. I'd certainly lean towards Google Maps, as they have a pretty established API and certainly a lot of documentation. Sending emails may be a bit of a stress point, as anonymous emailing would involve being able to both send and receive emails. It may be easiest to use a service like sendgrid or mailgun to drive emails. Sendgrid in particular supports the idea of "webhooks," which essentially lets you expose an endpoint on your server that Sendgrid will hit with new email data, as it comes in. 

### Shannon
In general, this project might be too similar to Grumblr but you should think about what additional features you can add to make it more challenging. Adding to that, I think that use of the Google Maps API should be a definite feature in the application. Perhaps you can do some social network integration or some advanced support for connecting or linking to other websites (since sometimes, students link to the official IKEA webpage to show off what dresser they have).

---

To view this file with formatting, visit the following page: https://github.com/CMU-Web-Application-Development/Team73/blob/master/feedback/proposal.md
