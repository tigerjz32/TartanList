Project Spec Feedback
=====================

### Product Backlog (10/10)
  * Looks good! You seem to have done a thorough job thinking out all the aspects of your project, and splitting these actions into well-defined groupings. 

### Data Models (10/10)
  * Are you planning on just using Django's built-in User model?
  * How are you going to store the location of an item? 
  * What is the itemURL referring to? Posts on For Sale @ CMU?

### Wireframes or Mock-ups (10/10)
  * You may want to consider making the "edit" page emphasize the edit form more (i.e. why are there search results on that page?). This way, the thing the user is supposed to do is emphasized.

### Sprint 1 Planning (10/10)
  * This looks good. It seems a little large, but I understand that a lot of this likely comes from Grumblr (user login / logout, for example). Be sure to think proactively about how much work you're leaving for future sprints. 
  
---

### Total: 40/40

Graded by: Salem Hilal (salem@cmu.edu)

To view this file with formatting, visit the following page: https://github.com/CMU-Web-Application-Development/Team73/blob/master/feedback/specification.md

